﻿(function()
{
	window.view=
	{
		init: function()
		{
			window.view.mapUI();
			window.view.appendWidgets();
			window.view.appendEvents();
			window.view.initValues();
		},
		
		mapUI: function()
		{
			window.view.gui=
			{
				system:
				{	
					popupInfo:
					{
						whole: $('#popupInfo'),
						message: $('#popupInfo .popup-text-details:first'),
						btn_ok: $('#popupInfo .btn-ok:first'),
					},
					
					pages:
					{
						login: $('#login-page'),
						employee: $('#employee-page'),
						customer: $('#customer-page'),
					},
				},
				login:
				{
					login: $('#page-login-input-login'),
					password: $('#page-login-input-password'),
					btn_login: $('#page-login-btn-login'),
				},
				tables:
				{
					project_list: $('#div-projects-list'),
					tasks_list: $('#div-tasks-list'),
					realizations_list_body: $('#realizations-list-body'),
				},
				buttons:
				{
					back_to_projects: $('#back-to-project'),
					back_to_tasks: $('#back-to-tasks'),
					
					add_realization: $('#add-realization'),
					cancel_add_realization: $('#cancel-add-realization'),
					add_new_realization: $('#add-new-realization'),
					
					add_task: $('#add-task'),
					check_realizations: $('#check-realizations'),
					cancel_add_task: $('#cancel-add-task'),
					add_new_task: $('#add-new-task'),
					
					add_project: $('#add-project'),
					check_tasks: $('#check-tasks'),
					cancel_add_project: $('#cancel-add-project'),
					add_new_project: $('#add-new-project'),
					
					back_to_customer_projects: $('#back-to-customer-project'),
					
					add_employee: $('#add-employee'),
					
					cancel_add_employee: $('#cancel-add-employee'),
					add_new_employee: $('#add-new-employee'),
					
					check_customer_tasks: $('#check-customer-tasks'),
				},
				divs:
				{
					project: $('#div-projects-list'),
					task: $('#div-task-list'),
					realizations: $('#div-realizations-list'),
					new_realization: $('#new-realization'),
					new_task: $('#new-task'),
					new_project: $('#new-project'),
					customer_project: $('#div-customer-projects-list'),
					customer_task: $('#div-customer-task-list'),
					employee_tabs: $('#employee-tabs'),
					new_employee: $('#new-employee'),
					div_employees_list: $('#div-employees-list'),
				},
			};
		},
		
		appendWidgets: function()
		{
			window.view.gui.system.popupInfo.whole.popup();
			window.view.gui.system.popupInfo.btn_ok.button();
			window.view.updateDateMasks();
			window.view.gui.divs.employee_tabs.tabs();
		},
		
		appendEvents: function()
		{
			window.view.gui.login.btn_login.on('click', window.view.handlers.handleLogIn);
			window.view.gui.buttons.add_realization.on('click', window.view.handlers.handleToAddNewRealization);
			window.view.gui.buttons.cancel_add_realization.on('click', window.view.handlers.handleAddCancelRealization);
			window.view.gui.buttons.add_new_realization.on('click', window.view.handlers.handleAddNewRealization);
			
			window.view.gui.buttons.add_task.on('click', window.view.handlers.handleToAddNewTask);
			window.view.gui.buttons.cancel_add_task.on('click', window.view.handlers.handleAddCancelTask);
			window.view.gui.buttons.add_new_task.on('click', window.view.handlers.handleAddNewTask);
			window.view.gui.buttons.check_realizations.on('click', window.view.handlers.handleCheckRealizations);
			
			window.view.gui.buttons.add_project.on('click', window.view.handlers.handleToAddNewProject);
			window.view.gui.buttons.cancel_add_project.on('click', window.view.handlers.handleAddCancelProject);
			window.view.gui.buttons.add_new_project.on('click', window.view.handlers.handleAddNewProject);
			window.view.gui.buttons.check_tasks.on('click', window.view.handlers.handleCheckTasks);
			
			window.view.gui.buttons.add_employee.on('click', window.view.handlers.handleAddEmployee);
			window.view.gui.buttons.add_new_employee.on('click', window.view.handlers.handleAddNewEmployee);
			window.view.gui.buttons.cancel_add_employee.on('click', window.view.handlers.handleCancelNewEmployee);
			
			window.view.gui.buttons.check_customer_tasks.on('click', window.view.handlers.handleCheckCustomerTasks);
			
			window.view.gui.buttons.back_to_projects.click(function(){
				window.view.gui.divs.project.show();
				window.view.gui.divs.task.hide();
			});
			window.view.gui.buttons.back_to_customer_projects.click(function(){
				window.view.gui.divs.customer_project.show();
				window.view.gui.divs.customer_task.hide();
			});
			window.view.gui.buttons.back_to_tasks.click(function(){
				window.view.gui.divs.task.show();
				window.view.gui.divs.realizations.hide();
			});
			$(document).ready(function () {
				$('#projects-list-body tr').hover(function () {
					  $(this).addClass('hover-tr');
				   }, function () {
					 $(this).removeClass('hover-tr');
				}).click( function() {
					$(this).addClass('clicked-tr');
					window.view.projectId = $(this).attr('value');
					if (window.view.projectId != "-1")
						window.view.gui.buttons.check_tasks.button('enable');
					$('#projects-list-body').children('tr').each(
						function (){
							if ($(this).attr('value') != window.view.projectId)
								$(this).removeClass('clicked-tr');
						});
					});
			});
			
			$(document).ready(function () {
				$('#tasks-list-body tr').hover(function () {
					  $(this).addClass('hover-tr');
				   }, function () {
					 $(this).removeClass('hover-tr');
				}).click( function() {
					$(this).addClass('clicked-tr');
					window.view.taskId = $(this).attr('value');
					if (window.view.taskId != "-1")
						window.view.gui.buttons.check_realizations.button('enable');
					$('#tasks-list-body').children('tr').each(
						function (){
							if ($(this).attr('value') != window.view.taskId)
								$(this).removeClass('clicked-tr');
						});
					});
			});
			
			$(document).ready(function () {
				$('#customer-projects-list-body tr').hover(function () {
					  $(this).addClass('hover-tr');
				   }, function () {
					 $(this).removeClass('hover-tr');
				}).click( function() {
					$(this).addClass('clicked-tr');
					window.view.customerProjectId = $(this).attr('value');
					if (window.view.customerProjectId != "-1")
						window.view.gui.buttons.check_customer_tasks.button('enable');
					$('#customer-projects-list-body').children('tr').each(
						function (){
							if ($(this).attr('value') != window.view.customerProjectId)
								$(this).removeClass('clicked-tr');
						});
					});
			});
			
			$(document).ready(function () {
				$('#customer-tasks-list-body tr').hover(function () {
					  $(this).addClass('hover-tr');
				   }, function () {
					 $(this).removeClass('hover-tr');
				}).click( function() {
					$(this).addClass('clicked-tr');
					window.view.customerTaskId = $(this).attr('value');
					if (window.view.customerTaskId != "-1")
						window.view.gui.buttons.check_customer_tasks.button('enable');
					$('#customer-tasks-list-body').children('tr').each(
						function (){
							if ($(this).attr('value') != window.view.customerTaskId)
								$(this).removeClass('clicked-tr');
						});
					});
			});
		},
		
		initValues: function()
		{
			window.view.projectId= "-1";
			window.view.taskId= "-1";
			window.view.realizationId= "-1";
			window.view.customerProjectId= "-1";
			window.view.customerTaskId= "-1";
		},

		handlers:
		{
			handleLogIn: function()
			{
				var login = window.view.gui.login.login.val();
				var password = window.view.gui.login.password.val();
				if ('employee' == login && 'employee' == password)
					$.mobile.changePage(window.view.gui.system.pages.employee, {allowSamePageTransition:true});
				if ('customer' == login && 'customer' == password)
					$.mobile.changePage(window.view.gui.system.pages.customer, {allowSamePageTransition:true});
			},
			
			handleToAddNewRealization: function()
			{
				window.view.gui.divs.realizations.hide();
				window.view.gui.divs.new_realization.show();
			},
			
			handleAddCancelRealization: function()
			{
				window.view.gui.divs.realizations.show();
				window.view.gui.divs.new_realization.hide();
			},
			
			handleAddNewRealization: function()
			{
				window.view.gui.divs.realizations.show();
				window.view.gui.divs.new_realization.hide();
			},
			
			handleToAddNewTask: function()
			{
				window.view.gui.divs.task.hide();
				window.view.gui.divs.new_task.show();
			},
			
			handleAddCancelTask: function()
			{
				window.view.gui.divs.task.show();
				window.view.gui.divs.new_task.hide();
			},
			
			handleAddNewTask: function()
			{
				window.view.gui.divs.task.show();
				window.view.gui.divs.new_task.hide();
			},
			
			handleToAddNewProject: function()
			{
				window.view.gui.divs.project.hide();
				window.view.gui.divs.new_project.show();
			},
			
			handleAddCancelProject: function()
			{
				window.view.gui.divs.project.show();
				window.view.gui.divs.new_project.hide();
			},
			
			handleAddNewProject: function()
			{
				window.view.gui.divs.project.show();
				window.view.gui.divs.new_project.hide();
			},
			
			handleCheckTasks: function()
			{
				window.view.gui.divs.project.hide();
				window.view.gui.divs.task.show();
			},
			
			handleCheckRealizations: function()
			{
				window.view.gui.divs.task.hide();
				window.view.gui.divs.realizations.show();
				window.view.gui.tables.realizations_list_body.html("");
				if (window.view.taskId == "1")
				{
					var tr = $(document.createElement('tr')).attr('value', 1).appendTo(window.view.gui.tables.realizations_list_body);
					$(document.createElement('td')).html("Przygotowanie szkieletu aplikacji").appendTo(tr);
					$(document.createElement('td')).html("2015-01-01").appendTo(tr);
					$(document.createElement('td')).html("02:00").appendTo(tr);
					tr = $(document.createElement('tr')).attr('value', 2).appendTo(window.view.gui.tables.realizations_list_body);
					$(document.createElement('td')).html("Przygotowanie widoku pracownika").appendTo(tr);
					$(document.createElement('td')).html("2015-01-01").appendTo(tr);
					$(document.createElement('td')).html("05:00").appendTo(tr);
				} else if (window.view.taskId == "2")
				{
					var tr = $(document.createElement('tr')).attr('value', 1).appendTo(window.view.gui.tables.realizations_list_body);
					$(document.createElement('td')).html("Ogólny projekt systemu").appendTo(tr);
					$(document.createElement('td')).html("2015-01-02").appendTo(tr);
					$(document.createElement('td')).html("04:00").appendTo(tr);
					tr = $(document.createElement('tr')).attr('value', 2).appendTo(window.view.gui.tables.realizations_list_body);
					$(document.createElement('td')).html("Przygotowanie diagramów klas").appendTo(tr);
					$(document.createElement('td')).html("2015-01-02").appendTo(tr);
					$(document.createElement('td')).html("04:00").appendTo(tr);
					tr = $(document.createElement('tr')).attr('value', 3).appendTo(window.view.gui.tables.realizations_list_body);
					$(document.createElement('td')).html("Przygotowanie diagramów komponentów").appendTo(tr);
					$(document.createElement('td')).html("2015-01-03").appendTo(tr);
					$(document.createElement('td')).html("03:00").appendTo(tr);
					tr = $(document.createElement('tr')).attr('value', 4).appendTo(window.view.gui.tables.realizations_list_body);
					$(document.createElement('td')).html("Przygotowanie diagramów sekwencji").appendTo(tr);
					$(document.createElement('td')).html("2015-01-03").appendTo(tr);
					$(document.createElement('td')).html("05:00").appendTo(tr);
					tr = $(document.createElement('tr')).attr('value', 5).appendTo(window.view.gui.tables.realizations_list_body);
					$(document.createElement('td')).html("Przygotowanie projektu bazy danych").appendTo(tr);
					$(document.createElement('td')).html("2015-01-04").appendTo(tr);
					$(document.createElement('td')).html("07:00").appendTo(tr);
				}
				
				$('#realizations-list-body tr').hover(function () {
					  $(this).addClass('hover-tr');
				   }, function () {
					 $(this).removeClass('hover-tr');
				}).click( function() {
					$(this).addClass('clicked-tr');
					window.view.realizationId = $(this).attr('value');
					if (window.view.realizationId != "-1")
						window.view.gui.buttons.check_realizations.button('enable');
					$('#realizations-list-body').children('tr').each(
						function (){
							if ($(this).attr('value') != window.view.realizationId)
								$(this).removeClass('clicked-tr');
						});
				});
			},
			
			handleAddEmployee: function()
			{
				window.view.gui.divs.div_employees_list.hide();
				window.view.gui.divs.new_employee.show();
			},
			
			handleCancelNewEmployee: function()
			{
				window.view.gui.divs.new_employee.hide();
				window.view.gui.divs.div_employees_list.show();
			},
			
			handleAddNewEmployee: function()
			{
				window.view.gui.divs.new_employee.hide();
				window.view.gui.divs.div_employees_list.show();
			},
			
			handleCheckCustomerTasks: function()
			{
				window.view.gui.divs.customer_project.hide();
				window.view.gui.divs.customer_task.show();
			},
		},
		
		updateDateMasks: function()
		{
			 $.datepicker.regional['pl'] = {
		                closeText: 'Zamknij',
		                prevText: '&#x3c;Poprzedni',
		                nextText: 'Następny&#x3e;',
		                currentText: 'Dziś',
		                monthNames: ['Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec',
		                'Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'],
		                monthNamesShort: ['Sty','Lu','Mar','Kw','Maj','Cze',
		                'Lip','Sie','Wrz','Pa','Lis','Gru'],
		                dayNames: ['Niedziela','Poniedzialek','Wtorek','Środa','Czwartek','Piątek','Sobota'],
		                dayNamesShort: ['Nie','Pn','Wt','Śr','Czw','Pt','So'],
		                dayNamesMin: ['N','Pn','Wt','Śr','Cz','Pt','So'],
		                weekHeader: 'Tydz',
		                dateFormat: 'yy-mm-dd',
		                firstDay: 1,
		                isRTL: false,
		                showMonthAfterYear: false,
		                yearSuffix: ''};
		    $.datepicker.setDefaults($.datepicker.regional['pl']);
			
			$(document).find('.date-mask').each(function(){
				if (this.type != 'date')
				{
					$(this).datepicker
					({
						yearRange: "-120:+10",
						dateFormat: 'yy-mm-dd',
						changeMonth: true,
					    changeYear: true,
					});
				}
			});
		},

	};
	
})(jQuery);

