﻿(function($, undefined)
{
	window.app=
	{
		init: function()
		{	
			window.view.init();
		},
		
		system: 
		{
			showInfoDialog: function(message)
			{
				$('.ui-loader').css('display','none');
				
				if(message instanceof Object)
				{  
					window.view.gui.system.popupInfo.message.text(message.description);
				}
				else
				{
					window.view.gui.system.popupInfo.message.text(message);
				}
				
				window.view.gui.system.popupInfo.whole.popup('open');
				window.view.gui.system.popupInfo.btn_ok.focus();
			},
		},
		
		
	};
	
})(jQuery);

